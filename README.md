# Service.DBImporter

Handles Database deployments from s3 to k8s

# What to put in k8s

Below is a snippet used for deployment of saved models using this docker image in kubernetes

## Postgres

```
spec:
  template:
    spec:
      containers:
      - name: deployer
        image: IMAGENAME
        command: ["/bin/sh","-c","gzip -d /db/postgres.sql.gz && psql -U postgres -h postgres -f /db/postgres.sql"]

      restartPolicy: Never
        
      imagePullSecrets:
      - name: regcred
```
Change IMAGENAME to the correct image name and the correct repository

This tool loads the postgres backup into a docker image, deploys it in k8s, unzips it then deploys it to the postgres database in the namespace. By default it uses the hostname postgres and the username postgres. The postgres backup included in this repo is for stockpiles from 2021. 

TODO: Replace this method with s3?

## Mongo

```
    spec:
      containers:
      - name: deployer
        image: IMAGENAME
        args:
        - -c 
        - mkdir /mongodb/backup/ &&
          s3cmd --no-check-certificate get s3://intellisense-mongo/stockpiles/$(KOLOMELA_BACKUP).gz &&
          gzip -d /mongo/backup/$(KOLOMELA_BACKUP).gz &&
          mongorestore --host mongo --port 27017 --db DATABASE_NAME $(KOLOMELA_BACKUP)
        command:
        - sh
        env:
        - name: 
          valueFrom: KOLOMELA_BACKUP
            configMapKeyRef:
              key: mongo_kolomela_s3
              name: mongo-backup-names
              optional: false
```

Change IMAGENAME to the correct image name and the correct repository

This image uses s3cmd to pull a specific version from s3 into the docker container. This is then used to deploy that backup using mongorestore. This script assumes multiple sites are being deployed and as such as a configMap that stores the names of the backup. This is then used to pull, unzip and deploy a specific version from s3. This is used as the database backups are far too large to package as docker images.